#!/home/clarc/pyenv/bin/python
# Tool to convert CSV files (with configurable delimiter and text wrap
# character) to Excel spreadsheets.
import string
import sys
import getopt
import re
import os
import os.path
import csv
import pickle
from xlwt import *


def convertToExcel(inputFile, outputDir):
  """ This is how we are called """
  workbook = Workbook()
  worksheet = workbook.add_sheet("Sheet 1")

  data = pickle.load(open(inputFile))
  header = data["dataList"][0].keys()
  header = ["List", "Name", "correct", "choice", "reaction time", "Description", "Statement"]
  metaData = data["metaData"]

  for item, col in zip(header, range(len(header))):
      worksheet.write(0, col, item)

  for line, row in zip(data["dataList"], range(len(data["dataList"]))):
      for item, col in zip(header, range(len(header))):
          worksheet.write(row + 1, col, line[item])

  outputFile = outputDir + "/" + str(metaData["subjectId"]) + "-" + metaData["subject name"] + ".xls"
  workbook.save(outputFile)

if __name__ == "__main__":
  convertToExcel(sys.argv[1])
