#!/home/clarc/pyenv/bin/python
import cgi
import pickle
from dataToExcel import convertToExcel

query = cgi.FieldStorage()
idNum = query.getvalue("id")

print "Content-type: application/json\n\n"

convertToExcel("../temp-data/" + idNum + ".pk", "../data")
