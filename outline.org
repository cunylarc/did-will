
* serverDataFetch() --> _gotDataEvent_
  returns data['practice', 'real']


* practiceTrial

** absorb(data)

** currentVideo()

** recordData() <-- _gotUserInputEvent_

** finished-p()
   returns true if repeated 5 times and more than %80 correct


* realTrial
** absorb(trialData, metaData)
   add metadata to interal notebook

** advance()
   set the pointer to the next video file

** currentVideo()
   returns the filename of the current video

** finished-p()
   returns true if finished

** giveData()
   allows user to download recorded data (for offline version)

** recordData() <-- _gotUserInputEvent_
   record experimental data collected from participant

** upload()
   saves data to server


* videoPlayer

** playInstructions()  --> _playbackEnded_
   play instructional video

** playVideo() --> _playbackEnded_
   play video preceeded by "get ready!" text

** acceptInput() <-- _playbackEnded_
** rejectInput() --> _gotUserInputEvent_
   turns off input capture


* timer

** start()

** stop()

** getTime()


* controller <-- _userStartsEvent_

** initialize objects
   practiceTrial, realTrial, videoPlayer

** show introduction (0)
   - videoPlayer.playInstructions()
   - wait for _playbackEnded_
   - _incrementExperimentState_

** run practiceTrial (1)
   - videoPlayer.playVideo(practiceTrial.currentVideo())
   - wait for --> _playBackEnded_
   - invoke acceptInput()
     - invoke rejectInput() --> _gotUserInputEvent_
   - _gotUserInputEvent_ --> recordData() [[http://snipplr.com/view/16002/jquery-event-pass-data/][passing data w/ events]]
   - check practiceTrial.finished-p()
   - if not, practiceTrial.advance() && go to step 1
   - if so, --> _incrementExperimentState_

** run realTrial (2)
   - videoPlayer.playVideo(realTrial.currentVideo())
   - wait for --> _playBackEnded_
   - invoke videoPlayer.acceptInput()
     - invoke videoPlayer.rejectInput() --> _gotUserInputEvent_
   - _gotUserInputEvent_ --> recordData() [[http://snipplr.com/view/16002/jquery-event-pass-data/][passing data w/ events]]
   - check realTrial.finished-p()
   - if not, realTrial.advance() && go to step 1
   - if so, --> _updateExperimentState()_

** finish up (3)
   - upload data to server
   - say goodbye


* controller

** state: 0

  - playbackEnded
    incrementState()

  - gotUserInputEvent
    nil

** state: 1
   experiment = practiceTrial

  - playbackEnded
    videoPlayer.acceptInput()

  - gotUserInputEvent
    if(!experiment.finished-p())
      experiment.recordData(data-from-event)
    else
      incrementState()

** state: 2
   experiment = realTrial

  - playbackEnded
    videoPlayer.acceptInput()

  - gotUserInputEvent
    if(!experiment.finished-p())
      experiment.recordData(data-from-event)
    else
      incrementState()

** state: 3
   upload data
{
  - playbackEnded
    nil

  - gotUserInputEvent
    nil
