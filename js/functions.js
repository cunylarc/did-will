// utility functions and whatnot

// logging
function report(text){
    if (debugging)
	console.log(text);
}


function fetchTrialData(listLetter, network){
    if(network){
	$.getJSON('./scripts/getList.cgi?list=' + listLetter, function(dataRecieved){
		      $().trigger({type:'fetchedData', d:dataRecieved});
		  });
    }
    else{
	var e = document.createElement("script");
	e.src = "trialLists/list-" + listLetter + ".js";
	report("source: " + e.src);
	e.type = "text/javascript";
	document.getElementsByTagName("head")[0].appendChild(e);
    }
}

// converts data to excel documents on the server
function xlify(){
    $.getJSON('./scripts/makeXl.cgi?id=' + realTrial.metaData.subjectId);

}



/* collects form data into info
 * info is passed by reference */
function collectFormData(info){
    $('#trialForm input').each(function(i, form){
				   info[$(form).attr('id')] = $(form).val();
			       });

    $('#trialForm select').each(function(i, select){
				    info[$(select).attr('id')] = $(select).val();
				    report(select);
				    report($(select).val());
				});
}

/* called from control.js.  temporarily takes over flow control,
 *  displays conditional instructions dependant on inputMethod,
 *  accepts (spacebar) input, advances through instructions and
 *  finally calls incrementStage()  */
function textInstructions(inputMethod){
    report("entering textInstructions");
    $('#trialForm > *').remove();
    $('#trialForm').html('Welcome to the experiment.<br><br> In this experiment you will see a series of video clips. <br> For each video clip you will be asked a question about the objects in the clip.  Choose the correct answer by ');
    if(inputMethod == 'keyboard'){
	$('#trialForm').append('pressing either the "Z" key (for the object on your left) or the  "M" key (for the object on your right). ');
    }else if(formData['inputMethod'] == 'touchscreen'){
	$('#trialForm').append('touching either the left or right object on the screen.');
    }
    else{
	$('#trialForm').append('clicking either the left or right object.');
    }
    $('#trialForm').append('<br><br>Press the SPACEBAR to continue with the instructions.');
    $('#trialForm').css({'text-align':'center', 'border-style':'none'});
    $().bind('keypress', function(e){
		 if(e.which == 32){
		     $().unbind('keypress');
		     report("binding spacebar the first time");
		     $('#trialForm').html('There are two sections in this experiment.<br/><br/> The first section is a training section. In the training section you will see a video clip and be asked a question about that clip. The computer will then let you know if you got that question correct or incorrect, and will show your overall score for the training section.<br/><br/>After you complete the training section you will press the SPACEBAR to proceed to the experiment. In the experiment you will not receive any feedback about your responses.<br/><br/>If you have any questions please ask the experimenter now.<br/><br/>When you are ready, press the SPACEBAR to start the training section.');
		     $('#trialForm').css({'text-align':'center', 'border-style':'none', 'font-size':'100%'});
		     $().bind('keypress', function(e){
				  if(e.which == 32){
				      $().unbind('keypress');
				      incrementStage();
				  }});
		 }});
}

// Displays WAITTEXT centered on screen, executes PRECALLBACK immediately,
// and executes POSTCALLBACK upon the user clicking spacebar
function waitForSpaceBar(waitText, preCallback, postCallback){
    report(waitText);
    report(preCallback);
    report(postCallback);
    $.loading(false);
    preCallback();
    $.loading(true, {text: waitText, align: "center", pulse: ""});
    $().bind('keypress', function(e){
		 if(e.which == 32){
		     $.loading(false);
		     $().unbind('keypress');
		     postCallback();
		 }
	     });
}

/* return the filename for the introduction instructional video
 * appropriate for the participant's age and method of input  */
function pickInstrVideo(age, input){
//    return videoDirectory + "quick-intro.ogg";
    report("age: " +  age);
    report("input method: " +  input);
    if(age == 'test'){
	return videoDirectory + "quick-intro.ogg";
    }
    else if(age == 'adult'){
	if(input == "keyboard")
	    return videoDirectory + "InstructionAdultPush.ogg";
	else
	    return videoDirectory + "InstructionAdultTouch.ogg";
    }
    else{
	if(input == "keyboard")
	    return videoDirectory + "InstructionChildPush.ogg";
	else
	    return videoDirectory + "InstructionChildTouch.ogg";
    }
}

function gotClick(video, e){
    var mouseX = e.clientX - video.offsetLeft;
    var vidWidth = video.clientWidth;
    var choice = ((mouseX < vidWidth / 2) ? "L" : "R");
    nextVid(choice, video);
}

function eraseTheReadyText(){
    if(paused){
	report("We are in eraseTheReadyText and are paused");
	waitForSpaceBar("Paused: Press Spacebar to resume",
			function(){},
			function(){
			    $().trigger('unpauseExperiment');
				  });
    }
    else{
	report("We are in eraseTheReadyText and are NOT paused");
	$.loading(false);
	// we need this delay so that artifacts from the happy face aren't
	// visible on the video when it transitions.
	setTimeout(function(){
		       if(!practicing){
			   // bindRestart();
		       }
		       $('#vid').unbind('canplaythrough');
		       report('PLAYING ' + $('#vid').attr('src'));
		       $('#vid').show();
		       $('#vid').trigger('play');
		       report("length: " + $('#vid').attr('duration')*1000);
		       var then = Date.now();
		       var vidLength = $('#vid').attr('duration')*1000;
		       /* when this timeout fires
			the video has finished. fire
			the videoEnded event so they
			know to start capturing
			input on this thing. */
		       setTimeout(function(){
				      report('video ended after ' + (Date.now() - then)); //+ ());
				      playQueue.pop();
				      if(restartedQueue.length > 0)
				      	  report('we are discarding this timeout. its from an additional restart');
				      else
				      	  $().trigger({type:'playbackEnded', timeThen:Date.now(), duration:vidLength});
				      restartedQueue.pop();
     				  }, vidLength);
		   }, 50);
	$().unbind('unpauseExperiment');
    }
}
