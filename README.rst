========================
 Did/Will Comprehension
========================

A JavaScript/HTML5 video experiment that test L1 and L2 learners'
comprehension of Did and Will.

The experiment is hosted at http://www.experiments.cunylarc.org/didwill/ . The
videos are not included in this directory. They're hosted at
http://www.cunylarc.org/videos/ogg/ .

TODO
====

- Include set up instructions.
- Use Flask instead of CGI scripts?

Overview
========

Participants are shown a video of an experimenter facing them with an object in
each of her hands and through narration and gesture does one the following:

- Has both items perform an action and then one of them stops. She asks "Show me
  the one that *was* _ing?"
- Has both items perform an action and then one of them stops. She asks "which
  one "Show me the one that *did* X?"
- Has both items perform an action and then one of them stops. She asks "which
  one "Show me the one that *is* Xing?"
- Describes an action or behaviour that both objects want to / plan to engage
  in, but then only one of them does it. She asks "Show me the one that *will*
  X".

The participant responds by touching the item on the screen or pressing a
button on a keyboard that corresponds to either the left or right object.

There is an online and offline verison. The offline version dumps json to
the screen at the end, and the experimenter saves the page to disk.

The online version saves the data to the server after each trial and runs some
CGI scripts that generate excel (xls) files at the end of the
experiment. Appending /data to the url will bring you to the directory
listing of `the excel files`_.

.. _`the excel files`: http://www.experiments.cunylarc.org/didwill/data/
